
create-database:
	docker compose exec phpfpm-app bin/console doctrine:database:create

create-schema:
	docker compose exec phpfpm-app bin/console doctrine:schema:create
