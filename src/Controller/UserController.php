<?php declare(strict_types=1);

namespace App\Controller;

use App\Domain\User\UserAggregate;
use App\Domain\User\UserId;
use EventSauce\EventSourcing\AggregateRootRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/user')]
final class UserController
{
    public function __construct(
        private AggregateRootRepository $userAggregateRepository,
    )
    {
    }

    #[Route(path: '/create', methods: ['GET'])]
    public function createUser (Request $request): JsonResponse
    {
        dump('createUser');
        $userId = Uuid::uuid4()->toString();
        $aggregateRoot = UserAggregate::createUser(UserId::fromString($userId), $request->query->get('name'), $request->query->get('email'));
        $this->userAggregateRepository->persist($aggregateRoot);

        return new JsonResponse(['user' => $userId]);
    }

    #[Route(path: '/update/{aggregateRootId}')]
    public function updateUser (Request $request, string $aggregateRootId): JsonResponse
    {
        dump('updateUser');
        /** @var UserAggregate $aggregateRoot */
        $userId = UserId::fromString($aggregateRootId);

        $aggregateRoot = UserAggregate::updateUser($userId, $request->query->get('name'), $request->query->get('email'));
        $this->userAggregateRepository->persist($aggregateRoot);

        // $aggregateRoot = $this->userAggregateRepository->retrieve($userId);
        //$aggregateRoot->updateUser($userId, $request->query->get('name'), $request->query->get('email'));

        return new JsonResponse(['user' => $aggregateRootId]);
    }
}
