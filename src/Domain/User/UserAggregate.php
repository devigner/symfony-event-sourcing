<?php declare(strict_types=1);

namespace App\Domain\User;

use EventSauce\EventSourcing\AggregateRoot;
use EventSauce\EventSourcing\AggregateRootBehaviour;
use EventSauce\EventSourcing\AggregateRootId;
use Generator;
use Ramsey\Uuid\Uuid;

class UserAggregate implements AggregateRoot
{
    use AggregateRootBehaviour;

    public static function createUser(UserId $id, string $username, string $email): self
    {
        $process = self::createNewInstance($id);
        $process->recordThat(new UserWasCreated(Uuid::fromString($id->toString()), $username, $email));

        return $process;
    }

    public static function updateUser(UserId $id, string $username, string $email): self
    {
        $process = self::createNewInstance($id);
        $process->recordThat(new UserWasUpdated(Uuid::fromString($id->toString()), $username, $email));

        return $process;
    }

    public function applyUserWasCreated(UserWasCreated $event): void
    {
        echo('applyUserWasCreated');
        dump($event);
    }

    public function applyUserWasUpdated(UserWasUpdated $event): void
    {
        echo('applyUserWasUpdated');
        dump($event);
    }

    public static function reconstituteFromEvents(AggregateRootId $aggregateRootId, Generator $events): static
    {
        dump('reconstituteFromEvents');
        $aggregateRoot = self::createNewInstance($aggregateRootId);

        /** @var object $event */
        foreach ($events as $event) {
            $aggregateRoot->apply($event);
        }

        $aggregateRoot->aggregateRootVersion = $events->getReturn() ?: 0;

        return $aggregateRoot;
    }
}
