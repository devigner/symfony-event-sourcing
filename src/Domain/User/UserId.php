<?php declare(strict_types=1);

namespace App\Domain\User;

use EventSauce\EventSourcing\AggregateRootId;

class UserId implements AggregateRootId
{
    private function __construct(private string $id)
    {
    }

    public function toString(): string
    {
        return $this->id;
    }

    public static function fromString(string $aggregateRootId): AggregateRootId
    {
        return new static($aggregateRootId);
    }
}
