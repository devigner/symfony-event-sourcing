<?php declare(strict_types=1);

namespace App\Domain\User;

use App\Entity\User;
use App\Repository\UserRepository;
use EventSauce\EventSourcing\EventConsumer;
use EventSauce\EventSourcing\Message;
use Psr\Log\LoggerInterface;

class UserWasCreatedConsumer extends EventConsumer
{
    public function __construct(
        private LoggerInterface $logger,
        private UserRepository $userRepository,
    )
    {
    }

    public function handleUserWasCreated(UserWasCreated $event, Message $message): void
    {
        $this->logger->debug('handleUserWasCreated');
        $payload = $event->toPayload();
        $user = new User($payload['userId'], $payload['email'], $payload['username']);
        $this->userRepository->add($user);
    }

    public function __invoke(UserWasCreated $message)
    {
        $this->handle($message);
    }
}
