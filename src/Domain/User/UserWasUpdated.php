<?php declare(strict_types=1);

namespace App\Domain\User;

use EventSauce\EventSourcing\Serialization\SerializablePayload;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UserWasUpdated implements SerializablePayload
{
    public function __construct(
        private UuidInterface $userId,
        private string $username,
        private string $email,
    )
    {
    }

    public function toPayload(): array
    {
        return [
            'userId' => $this->userId->toString(),
            'username' => $this->username,
            'email' => $this->email,
        ];
    }

    public static function fromPayload(array $payload): SerializablePayload
    {
        return new UserWasUpdated(
            Uuid::fromString($payload['userId']),
            $payload['username'],
            $payload['email'],
        );
    }
}
