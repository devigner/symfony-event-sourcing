<?php declare(strict_types=1);

namespace App\Domain\User;

use App\Entity\User;
use App\Repository\UserRepository;
use EventSauce\EventSourcing\EventConsumer;
use EventSauce\EventSourcing\Message;
use Psr\Log\LoggerInterface;

class UserWasUpdatedConsumer extends EventConsumer
{
    public function __construct(
        private LoggerInterface $logger,
        private UserRepository $userRepository,
    )
    {
    }

    public function handleUserWasUpdated(UserWasUpdated $event, Message $message): void
    {
        $this->logger->debug('handleUserWasUpdated');
        $payload = $event->toPayload();
        /** @var User $user */
        $user = $this->userRepository->findBy(['id' => $payload['userId']]);
        $user->setEmail($payload['email']);
        $user->setName($payload['username']);
        $this->userRepository->add($user);
    }

    public function __invoke(UserWasUpdated $message)
    {
        $this->handle($message);
    }
}
